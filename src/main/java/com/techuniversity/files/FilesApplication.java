package com.techuniversity.files;

import com.techuniversity.files.storage.StorageProperties;
import com.techuniversity.files.storage.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
//@EnableConfigurationProperties(StorageProperties.class)
public class FilesApplication {

	public static void main(String[] args) {

		SpringApplication.run(FilesApplication.class, args);
	}
	// Se asterica y se inicializa desde FileSystemSotrageService pero no borramos.
	/*@Bean
	CommandLineRunner init(StorageService storageService) {
		return (args) -> {
			storageService.init();;
			storageService.deleteAll();
		};
	}*/

}
